#!/bin/sh

# Script for Apertis OStree root FS mount allowing to use
# `chroot` and `systemd-nspawn` commands during build time.

# Based on original script from OStree upstream:
# https://github.com/ostreedev/ostree/blob/master/src/switchroot/switchroot.sh

set -eu

sysroot=/scratch/mnt

os=apertis

bootconf=$sysroot/boot/loader/entries/ostree-apertis-0.conf

if [ ! -f "$bootconf" ]; then
    echo "OStree setup is not available!"
    exit 1
fi

ostree=$(grep -o 'ostree=[/.[:alnum:]]\+' $bootconf)
ostree=${ostree#*=}
# Due symlink
ostree=$(realpath $sysroot$ostree)

mkdir -p $ostree/boot/efi

## /etc/machine-id must be writable to allow to work systemd-nspawn
# but original `machine-id` file must stay empty in build time.
touch /tmp/machine-id
mount --bind /tmp/machine-id $ostree/etc/machine-id

systemd-nspawn -D $ostree systemd-machine-id-setup

# install EFI
systemd-nspawn \
  --bind $sysroot/boot:/boot \
  --bind $sysroot/boot/efi:/boot/efi \
  --bind $sysroot/ostree/deploy/$os/var:/var \
  -D $ostree bootctl --path=/boot/efi install

umount $ostree/etc/machine-id

# Copy config, kernel and initrd
# Change the name of config to unify with patch added in T4469
rsync -Pav $sysroot/boot/ostree $sysroot/boot/efi/
cp $sysroot/boot/loader/entries/ostree-apertis-0.conf $sysroot/boot/efi/loader/entries/ostree-0-1.conf
rm -f $sysroot/boot/efi/loader/loader.conf
