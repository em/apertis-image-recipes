release = "18.03"

properties(
    [
        pipelineTriggers([cron('H H(1-4) * * *')]),
    ]
)

def uploadDirectory(source, target, upload = true) {

  if (!upload) {
    println "Skipping upload of ${source} to ${target}"
    return
  }

  sshagent (credentials: [ "5a23cd79-e26d-41bf-9f91-d756c131b811", ] ) {
    env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
    env.NSS_WRAPPER_GROUP = '/dev/null'
    sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
    sh(script: "LD_PRELOAD=libnss_wrapper.so rsync -e \"ssh -oStrictHostKeyChecking=no\" -aP ${source} archive@images.apertis.org:/srv/images/public/${target}/")
  }
}

def runTestsJobs(version, release, type, arch, board, submit = true) {

  if (!submit) {
    println "Skipping submitting tests jobs"
    return
  }

  dir ("apertis-tests") {
    git(url: "https://git.apertis.org/cgit/apertis-tests.git/",
        poll: false,
        branch: "master")
  }

  // TODO: Remove "uefi" once images run in the actual boards
  def boardp = (board in ["uefi", "sdk"]) ? "qemu" : board

  withCredentials([ file(credentialsId: 'apertis-lava-user', variable: 'lqaconfig') ]) {
    sh(script: """\
    /usr/bin/lqa -c ${lqaconfig} submit \
    --profile apertis-${release}-${type}-${arch}-${boardp} \
    -g apertis-tests/templates/profiles.yaml \
    -t image_date:${version}""")
  }

}

def buildImage(architecture, type, board, debosarguments = "", sysroot = false, ostree = false, production = false) {
  return {
    node("docker-slave") {
      checkout scm
      docker.withRegistry('https://docker-registry.apertis.org') {
        buildenv = docker.image("docker-registry.apertis.org/apertis/apertis-${release}-image-builder")
        /* Pull explicitely to ensure we have the latest */
        buildenv.pull()

        buildenv.inside("--device=/dev/kvm") {
          stage("setup ${architecture} ${type}") {
            env.PIPELINE_VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
            sh ("env ; mkdir -p ${PIPELINE_VERSION}/${architecture}/${type}")
          }

          try {
              stage("${architecture} ${type} ospack") {
                   sh(script: """\
                   cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                   debos ${debosarguments} \
                    -t type:${type} \
                    -t architecture:${architecture} \
                    -t suite:${release} \
                    -t timestamp:${PIPELINE_VERSION} \
                    -t demopack:https://images.apertis.org/media/multimedia-demo.tar.gz \
                    -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                    ${WORKSPACE}/apertis-ospack.yaml""")
              }

              stage("${architecture} ${type} ospack upload") {
                uploadDirectory (env.PIPELINE_VERSION, "daily/${release}", production)
              }

              if (sysroot) {
                sysrootfile = "sysroot-apertis-${release}-${architecture}-${env.PIPELINE_VERSION}.tar.gz"
                stage("${architecture} sysroot tarball") {
                       sh(script: """\
                       mkdir -p sysroot/${release}; \
                       cd sysroot/${release}; \
                       cp -l ${WORKSPACE}/${PIPELINE_VERSION}/${architecture}/${type}/ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz .; \
                       debos ${debosarguments} \
                       -t architecture:${architecture} \
                       -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                       -t sysroot:${sysrootfile} \
                       ${WORKSPACE}/apertis-sysroot.yaml; \
                       rm ospack*""")
                 }
                 stage("${architecture} generate sysroot metadata") {
                       writeFile file: "sysroot/${release}/sysroot-apertis-${release}-${architecture}", text: "version=${release} ${PIPELINE_VERSION}\nurl=https://images.apertis.org/sysroot/${release}/${sysrootfile}"
                 }
                 stage("${architecture} sysroot upload") {
                  uploadDirectory ("sysroot/${release}/*", "sysroot/${release}", production)
                 }
              }

              stage("${architecture} ${type} ${board} image") {
                   sh(script: """\
                   cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                   debos ${debosarguments} \
                    -t architecture:${architecture} \
                    -t type:${type} \
                    -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                    -t image:apertis_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
                    ${WORKSPACE}/apertis-image-${board}.yaml""")
              }

              stage("${architecture} ${type} ${board} image upload") {
                uploadDirectory (env.PIPELINE_VERSION, "daily/${release}", production)
              }

              stage("Submitting tests jobs (lava)") {
                runTestsJobs (env.PIPELINE_VERSION, release, type, architecture, board, production)
              }

              if (ostree) {
                  buildOStree(architecture, type, board, debosarguments, production)
                  buildOStreeImage(architecture, type, board, debosarguments, production)
                  /* Create ostree and ospack for container (board name = lxc) */
                  buildContainer(architecture, type, "lxc", debosarguments, production)
              }

          } finally {
            stage("Cleanup ${architecture} ${type}") {
              deleteDir()
            }
          }
        }
      }
    }
  }
}

def buildOStree(architecture, type, board, debosarguments = "", production = false) {
    stage("${architecture} ${type} ${board} OStree repo pull") {
        // Allow to work with not-existing branches
        // ostree-push below refuses to update ostree branch if `ostree pull` failed by any other reason
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           rm -rf repo; \
           mkdir repo; \
           ostree init --repo=repo --mode archive-z2;
           ostree remote --repo=repo add --no-gpg-verify origin https://images.apertis.org/ostree/repo;
           ostree pull --repo=repo origin apertis/${release}/${architecture}-${board}/${type} || true""")
    }

    stage("${architecture} ${type} ${board} OStree commit") {
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           debos ${debosarguments} \
           -t architecture:${architecture} \
           -t type:$type \
           -t board:$board \
           -t suite:$release \
           -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
           -t message:${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
           ${WORKSPACE}/apertis-ostree-commit.yaml;
           ostree --repo=repo summary -u""")
    }

    stage("${architecture} ${type} ${board} OStree push") {
        if (!production) {
            println "Skipping push of OStree to apertis/${architecture}-${board}/${type}"
            return
        }
        sshagent (credentials: [ "5a23cd79-e26d-41bf-9f91-d756c131b811", ] ) {
            env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
                env.NSS_WRAPPER_GROUP = '/dev/null'
                sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
                sh(script: "LD_PRELOAD=libnss_wrapper.so ostree-push --repo ${env.PIPELINE_VERSION}/${architecture}/${type}/repo/ archive@images.apertis.org:/srv/images/public/ostree/repo/ apertis/${release}/${architecture}-${board}/${type}")
        }
    }
}


def buildOStreeImage(architecture, type, board, debosarguments = "", production = false) {

    if (architecture in ["arm64", "armhf"]) {
      println "Skipping ostree image for ${architecture}"
      return
    }

    stage("${architecture} ${type} ${board} OStree image build") {
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           debos ${debosarguments} \
           -t architecture:${architecture} \
           -t type:$type \
           -t board:$board \
           -t suite:$release \
           -t ospack:ospack_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
           -t image:apertis_ostree_${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
           -t message:${release}-${type}-${architecture}-${board}_${PIPELINE_VERSION} \
           ${WORKSPACE}/apertis-ostree-image-${board}.yaml;""")
    }

    stage("${architecture} ${type} ${board} OStree image upload") {
        uploadDirectory (env.PIPELINE_VERSION, "daily/${release}", production)
    }
}

def buildContainer(architecture, type, board, debosarguments = "", production = false) {

    buildOStree(architecture, type, board, debosarguments, production)

    stage("${architecture} ${type} ${board} OStree pack") {
        sh(script: """\
           cd ${PIPELINE_VERSION}/${architecture}/${type}; \
           debos ${debosarguments} \
           -t architecture:${architecture} \
           -t type:$type \
           -t suite:$release \
           -t repourl:https://images.apertis.org/ostree/repo \
           -t osname:apertis \
           -t branch:apertis/$release/${architecture}-${board}/${type} \
           -t ospack:lxc-ostree-${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
           -t message:${release}-${type}-${architecture}-${type}-${board}_${PIPELINE_VERSION} \
           ${WORKSPACE}/apertis-ostree-pack.yaml""")
    }

    stage("${architecture} ${type} ${board} OStree upload") {
        uploadDirectory ("${PIPELINE_VERSION}/${architecture}/${type}/lxc-ostree-${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz",
                         "lxc/${release}/${PIPELINE_VERSION}", production)
    }
}

/* Determine whether to run uploads based on the prefix of the job name; in
 * case of apertis we expect the official jobs under apertis-<release>/ while
 * non-official onces can be in e.g. playground/ */
def production = env.JOB_NAME.startsWith("apertis-")

def images = [:]

images["Sdk"] = buildImage("amd64", "sdk", "sdk",
                           "--scratchsize 10G",
                           false, false, production)

// Types for all boards, common debos arguments, sysroots and ospacks
def  types = [ [ "minimal", "", false, true],
               [ "target", "", false, true],
               [ "development", "--scratchsize 10G", true, false]
            ]

images += types.collectEntries { [ "Amd64 ${it[0]}": buildImage("amd64",
                                   it[0],
                                   "uefi",
                                   it[1],
                                   it[2],
                                   it[3],
                                   production ) ] }

images += types.collectEntries { [ "Arm64 ${it[0]}": buildImage("arm64",
                                   it[0],
                                   "uboot",
                                   it[1],
                                   it[2],
                                   it[3],
                                   production ) ] }

images += types.collectEntries { [ "Armhf ${it[0]}": buildImage("armhf",
                                   it[0],
                                   "uboot",
                                   it[1],
                                   it[2],
                                   it[3],
                                   production ) ] }

parallel images
